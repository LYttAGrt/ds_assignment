ZDFS File System - client
=========================
ZDFS client based on python.

Links to other projects
-----------------------
* namenode (kotlin) [https://gitlab.com/innofba/ds-name-node](https://gitlab.com/innofba/ds-name-node)
* datanode (python) [https://gitlab.com/emuravev/ds-data-node](https://gitlab.com/emuravev/ds-data-node)

Contribution
------------
* Iskender Guseinov - client.
* Evgeniy Muravev - datanode.
* Boris Floka - namenode.

How to run and use
------------------
    git clone https://gitlab.com/LYttAGrt/ds_assignment
    cd ds_assignment
    chmod +x client.py
    ./client.py name-node-host name-node-port [CMD]

Architecture
------------
![Architecture](https://imgur.com/kU9aPay.png)

As it presented here client and namenode can communicate with datanodes. 
But client can only get files from datanodes, whereas namenode can manipulate (make all required operations) with files and directories on datanodes.
While namenode and datanodes are located in the private subnet, client can be connected to the namenode from outer net via public IP address.

Communication protocol
----------------------
1. Client sends the HTTP request to namenode.
2. If request contains the command that namenode can execute by itself, such as `ls` (list files in directory):
3. then, client gets the response directly from namenode.
4. Otherwise, namenode redirects the request to corresponding datanode:
5. then, datanode sends the response to the client and log data to the namenode.

